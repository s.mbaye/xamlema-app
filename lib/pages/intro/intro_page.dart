import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:page_transition/page_transition.dart';
import 'package:smooth_page_indicator/smooth_page_indicator.dart';

import '../signin/signup_page.dart';

class IntroPage extends StatefulWidget {
  const IntroPage({Key? key}) : super(key: key);

  @override
  State<IntroPage> createState() => _IntroPageState();
}

class _IntroPageState extends State<IntroPage> {
  final _controller = PageController();
  @override
  void dispose() {
    _controller.dispose();
    super.dispose();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        backgroundColor: const Color(0xff0b8e36),
        body: PageView(
          controller: _controller,
          children: [
            Stack(
              alignment: AlignmentDirectional.center,
              children: [
                Positioned(
                  left: 326.w,
                  top: 87.h,
                  child: InkWell(
                    onTap: () => Navigator.pushReplacement(
                        context,
                        PageTransition(
                            child: const Signup(),
                            type: PageTransitionType.fade)),
                    child: Text("Passer",
                        style: TextStyle(color: Colors.white, fontSize: 16.sp)),
                  ),
                ),
                Positioned(
                  top: 188.h,
                  child: InkWell(
                    onTap: () => _controller.jumpToPage(2),
                    child: Image.asset("images/QR Code Illustration.png"),
                  ),
                ),
                Positioned(
                  top: 499.h,
                  child: SmoothPageIndicator(
                      controller: _controller,
                      count: 3,
                      effect: ScaleEffect(
                          activeStrokeWidth: 2,
                          activePaintStyle: PaintingStyle.stroke,
                          paintStyle: PaintingStyle.stroke,
                          dotColor: Colors.grey,
                          dotHeight: 6.h,
                          dotWidth: 6.w,
                          activeDotColor: Colors.white)),
                ),
                Positioned(
                  top: 533.h,
                  child: Container(
                    width: 375.w,
                    height: 300.h,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40.r),
                          topRight: Radius.circular(40.r)),
                      color: Theme.of(context).cardColor,
                    ),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 48.h,
                        ),
                        Text(
                          "Ak Xamlema",
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 28.sp,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 13.h,
                        ),
                        Text(
                          "Acheter, parrainer, c'est que des avantages!",
                          style: TextStyle(
                            color: const Color(0xff8F92A1),
                            fontSize: 18.sp,
                          ),
                        ),
                        SizedBox(
                          height: 54.h,
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: const Color(0xff0b8e36),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(16.r)),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 150.w, vertical: 18.h)),
                          onPressed: () => _controller.nextPage(
                              duration: const Duration(milliseconds: 400),
                              curve: Curves.easeInOut),
                          child: Text(
                            "Suivant",
                            style: TextStyle(fontSize: 16.sp),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ), // ! page 1
            Stack(
              alignment: AlignmentDirectional.center,
              children: [
                Positioned(
                  left: 326.w,
                  top: 87.h,
                  child: InkWell(
                    onTap: () => Navigator.pushReplacement(
                        context,
                        PageTransition(
                            child: const Signup(),
                            type: PageTransitionType.fade)),
                    child: Text("Passer",
                        style: TextStyle(color: Colors.white, fontSize: 16.sp)),
                  ),
                ),
                Positioned(
                  top: 188.h,
                  child: InkWell(
                    onTap: () => _controller.jumpToPage(2),
                    child: Image.asset("images/Face ID Illustration.png"),
                  ),
                ),
                Positioned(
                  top: 499.h,
                  child: SmoothPageIndicator(
                      controller: _controller,
                      count: 3,
                      effect: ScaleEffect(
                          activeStrokeWidth: 2,
                          activePaintStyle: PaintingStyle.stroke,
                          paintStyle: PaintingStyle.stroke,
                          dotColor: Colors.grey,
                          dotHeight: 6.h,
                          dotWidth: 6.w,
                          activeDotColor: Colors.white)),
                ),
                Positioned(
                  top: 533.h,
                  child: Container(
                    width: 375.w,
                    height: 300.h,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40.r),
                          topRight: Radius.circular(40.r)),
                      color: Theme.of(context).cardColor,
                    ),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 48.h,
                        ),
                        Text(
                          "Ak Xamlema",
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 28.sp,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 13.h,
                        ),
                        Text(
                          "Gagnez des bonus de fidélité à chaque \nachat chez vos partenaires.",
                          style: TextStyle(
                            color: const Color(0xff8F92A1),
                            fontSize: 18.sp,
                          ),
                        ),
                        SizedBox(
                          height: 54.h,
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: const Color(0xff0b8e36),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(16.r)),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 150.w, vertical: 18.h)),
                          onPressed: () => _controller.nextPage(
                              duration: const Duration(milliseconds: 400),
                              curve: Curves.easeInOut),
                          child: Text(
                            "Suivant",
                            style: TextStyle(fontSize: 16.sp),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ), //! page2

            Stack(
              alignment: AlignmentDirectional.center,
              children: [
                Positioned(
                  left: 326.w,
                  top: 87.h,
                  child: InkWell(
                    onTap: () => Navigator.pushReplacement(
                        context,
                        PageTransition(
                            child: const Signup(),
                            type: PageTransitionType.fade)),
                    child: Text("Passer",
                        style: TextStyle(color: Colors.white, fontSize: 16.sp)),
                  ),
                ),
                Positioned(
                  top: 188.h,
                  child: InkWell(
                    onTap: () => _controller.jumpToPage(2),
                    child: Image.asset("images/Illustration-3.png"),
                  ),
                ),
                Positioned(
                  top: 499.h,
                  child: SmoothPageIndicator(
                      controller: _controller,
                      count: 3,
                      effect: ScaleEffect(
                          activeStrokeWidth: 2,
                          activePaintStyle: PaintingStyle.stroke,
                          paintStyle: PaintingStyle.stroke,
                          dotColor: Colors.grey,
                          dotHeight: 6.h,
                          dotWidth: 6.w,
                          activeDotColor: Colors.white)),
                ),
                Positioned(
                  top: 533.h,
                  child: Container(
                    width: 375.w,
                    height: 300.h,
                    decoration: BoxDecoration(
                      borderRadius: BorderRadius.only(
                          topLeft: Radius.circular(40.r),
                          topRight: Radius.circular(40.r)),
                      color: Theme.of(context).cardColor,
                    ),
                    child: Column(
                      children: [
                        SizedBox(
                          height: 48.h,
                        ),
                        Text(
                          "Ak Xamlema",
                          style: TextStyle(
                              color: Theme.of(context).primaryColor,
                              fontSize: 28.sp,
                              fontWeight: FontWeight.bold),
                        ),
                        SizedBox(
                          height: 13.h,
                        ),
                        Text(
                          "Gagnez jusqu'à des millions de fcfa\npour tout achat d'un de vos filleuls.",
                          style: TextStyle(
                            color: const Color(0xff8F92A1),
                            fontSize: 18.sp,
                          ),
                        ),
                        SizedBox(
                          height: 54.h,
                        ),
                        ElevatedButton(
                          style: ElevatedButton.styleFrom(
                              backgroundColor: const Color(0xff0b8e36),
                              shape: RoundedRectangleBorder(
                                  borderRadius: BorderRadius.circular(16.r)),
                              padding: EdgeInsets.symmetric(
                                  horizontal: 130.w, vertical: 18.h)),
                          onPressed: () => Navigator.pushReplacement(
                              context,
                              PageTransition(
                                  child: const Signup(),
                                  type: PageTransitionType.fade)),
                          child: Text(
                            "Let's go !",
                            style: TextStyle(fontSize: 16.sp),
                          ),
                        ),
                      ],
                    ),
                  ),
                ),
              ],
            ), //! page3
          ],
        ));
  }
}
