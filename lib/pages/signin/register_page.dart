import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

import 'login_page.dart';
import 'register_name_page.dart';

class Registrar extends StatelessWidget {
  const Registrar({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
        //backgroundColor: Theme.of(context).cardColor,
      backgroundColor: Color(0xFFFFFFFF),
        body: SingleChildScrollView(
          child: Stack(
            children: [
              Padding(
                  padding: EdgeInsets.only(top: 60.h, left: 105.w),
                  child: Image.asset("images/Logo-Horizontale-200.png")),
              Padding(
                padding: EdgeInsets.only(
                  top: 220.h,
                  left: 16.w,
                  right: 16.w,
                ),
                child: TextFormField(
                  textInputAction: TextInputAction.done,
                  decoration: InputDecoration(
                    hintText: 'Votre adresse e-mail',
                    hintStyle: GoogleFonts.dmSans(
                      fontSize: 16.sp,
                      color: Colors.grey,
                    ),
                  ),
                  style: GoogleFonts.dmSans(
                      color: Theme.of(context).primaryColor,
                      fontWeight: FontWeight.bold),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 283.h, left: 16.w),
                child: Text(
                  "En créant un compte, vous acceptez nos conditions",
                  style: GoogleFonts.dmSans(
                    fontSize: 14.sp,
                    color: Colors.grey,
                  ),
                ),
              ),
              Padding(
                padding: EdgeInsets.only(top: 325.h, left: 14.w),
                child: ElevatedButton(
                    style: ElevatedButton.styleFrom(
                        backgroundColor: const Color(0xff0b8e36),
                        shape: RoundedRectangleBorder(
                            borderRadius: BorderRadius.circular(16.r)),
                        padding: EdgeInsets.symmetric(
                            horizontal: 140.w, vertical: 18.h)),
                    onPressed: () {
                      Navigator.push(
                          context,
                          PageTransition(
                              child: const NameRegister(),
                              type: PageTransitionType.fade));
                    },
                    child: Text("Continue", style: TextStyle(fontSize: 16.sp))),
              ),
              Padding(
                  padding: EdgeInsets.only(top: 410.h, left: 20.w),
                  child: InkWell(
                    onTap: () => Navigator.push(
                        context,
                        PageTransition(
                            child: const LoginPage(), type: PageTransitionType.fade)),
                    child: RichText(
                        text: TextSpan(children: [
                      TextSpan(
                          text: "Vous avez déjà un compte ? ",
                          style: GoogleFonts.dmSans(
                              fontSize: 16.sp, color: Colors.grey)),
                      TextSpan(
                        text: "Se connecter",
                        style: GoogleFonts.dmSans(
                            fontSize: 16.sp,
                            fontWeight: FontWeight.bold,
                            color: const Color(0xff105D38),
                            decoration: TextDecoration.underline),
                      )
                    ])),
                  )),
            ],
          ),
        ));
  }
}
