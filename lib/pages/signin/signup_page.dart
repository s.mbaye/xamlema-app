import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:page_transition/page_transition.dart';

import 'login_page.dart';
import 'register_page.dart';

class Signup extends StatelessWidget {
  const Signup({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: const Color(0xFFFFFFFF),
      body: Stack(
        alignment: AlignmentDirectional.topCenter,
        children: [
          Positioned(
            top: 20,
            child: Image.asset(
              "images/Logo-Horizontale-200.png",
              fit: BoxFit.fitWidth,
              width: MediaQuery.of(context).size.width,
            ),
          ),
          Positioned(
            top: 200.h,
            child: Image.asset(
              "images/intro_anime.gif",
              fit: BoxFit.fitWidth,
              width: MediaQuery.of(context).size.width,
            ),
          ),
          Positioned(
            bottom: 230.h,
            child: Center(
              child: Text(
                "Une nouvelle façon \nde tout se payer !",
                style: TextStyle(
                    color: Colors.black,
                    fontSize: 28.sp,
                    fontWeight: FontWeight.bold),
              ),
            ),
          ),
          Positioned(
            bottom: 140.h,
            child: ElevatedButton(
              style: ElevatedButton.styleFrom(
                  backgroundColor: const Color(0xff0b8e36),
                  shape: RoundedRectangleBorder(
                      borderRadius: BorderRadius.circular(16.r)),
                  padding:
                      EdgeInsets.symmetric(horizontal: 100.w, vertical: 18.h)),
              onPressed: () {
                Navigator.pushReplacement(
                    context,
                    PageTransition(
                        child: const LoginPage(), type: PageTransitionType.fade));
              },
              child: Text(
                "Se connecter",
                style: TextStyle(fontSize: 16.sp),
              ),
            ),
          ),
          Positioned(
            bottom: 110.h,
            child: InkWell(
              onTap: () => Navigator.pushReplacement(
                  context,
                  PageTransition(
                    child: const Registrar(),
                    type: PageTransitionType.fade,
                  )),
              child: Text(
                "S'inscrire",
                style: TextStyle(fontSize: 16.sp, color: Colors.black),
              ),
            ),
          ),
        ],
      ),
    );
  }
}
