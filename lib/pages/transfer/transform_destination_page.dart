import 'dart:ffi';

import 'package:flutter/material.dart';
import 'package:flutter_screenutil/flutter_screenutil.dart';
import 'package:flutter_svg/flutter_svg.dart';
import 'package:google_fonts/google_fonts.dart';
import 'package:page_transition/page_transition.dart';

import 'transfer_wally_page.dart';
import 'transform_bank_page.dart';

class TransformDsetination extends StatelessWidget {
  const TransformDsetination({Key? key}) : super(key: key);

  @override
  Widget build(BuildContext context) {

    final ColorScheme colorScheme = Theme.of(context).colorScheme;
    final Color oddItemColor = colorScheme.primary.withOpacity(0.05);
    final Color evenItemColor = colorScheme.primary.withOpacity(0.15);
    const int tabsCount = 2;

    return Scaffold(
      backgroundColor: const Color(0xff105D38),
      body: Stack(
        children: [
          Padding(
            padding: EdgeInsets.only(left: 10.0.w),
            child: Image.asset("images/Background.png"),
          ),
          Padding(
            padding: EdgeInsets.only(left: 98.w, top: 66.h),
            child: Text(
              "Parrainage",
              style: GoogleFonts.dmSans(
                  fontSize: 20.sp,
                  fontWeight: FontWeight.bold,
                  color: Colors.white),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(left: 16.w, top: 124.h),
            child: Text(
              "Choisissez une méthode de transfert ?",
              style: GoogleFonts.dmSans(fontSize: 18.sp, color: Colors.white),
            ),
          ),
          Padding(
              padding: EdgeInsets.only(left: 16.w, top: 60.h),
              child: InkWell(
                onTap: () {
                  Navigator.pop(context);
                },
                child: Container(
                  width: 40.w,
                  height: 40.h,
                  decoration: BoxDecoration(
                      borderRadius: BorderRadius.circular(12.r),
                      border: Border.all(width: 1, color: Colors.grey)),
                  child: const Icon(
                    Icons.navigate_before,
                    color: Colors.white,
                  ),
                ),
              )),
          Padding(
            padding: EdgeInsets.only(top: 173.h),
            child: Container(
              width: 375.w,
              height: 639.h,
              decoration: BoxDecoration(
                color: Theme.of(context).cardColor,
                borderRadius: BorderRadius.only(
                    topLeft: Radius.circular(30.r),
                    topRight: Radius.circular(30.r)),
              ),
              child: Column(
                children: [
                  SizedBox(
                    height: 16.h,
                  ),
                  Image.asset("images/home_indicator.png"),
                  SizedBox(
                    height: 20.h,
                  ),
                  SizedBox(
                    height: 16.h,
                  ),
                ],
              ),
            ),
          ),
          Padding(
            padding: EdgeInsets.only(top: 222.h, left: 10.w),
            child: DefaultTabController(
              initialIndex: 1,
              length: tabsCount,
              child: Container(
                width: 350.w,
                child: Scaffold(
                  appBar: AppBar(
                    toolbarHeight: 0,
                    automaticallyImplyLeading: false,
                    backgroundColor: Color(0xff008c36),
                    notificationPredicate: (ScrollNotification notification) {
                      return notification.depth == 1;
                    },
                    // The elevation value of the app bar when scroll view has
                    // scrolled underneath the app bar.
                    scrolledUnderElevation: 4.0,
                    shadowColor: Theme.of(context).shadowColor,
                    bottom: TabBar(
                      tabs: <Widget>[
                        Tab(
                          icon: const Icon(Icons.person),
                          text: "Parrainer un(e) proche",
                        ),
                        Tab(
                          icon: const Icon(Icons.beach_access_sharp),
                          text: "Mes filleuls",
                        ),
                      ],
                    ),
                  ),
                  body: TabBarView(
                    children: <Widget>[
                      Container(
                        child: Text("Formulaire de parrainage ici"),
                      ),
                      ListView.builder(
                        itemCount: 25,
                        itemBuilder: (BuildContext context, int index) {
                          return ListTile(
                            tileColor: index.isOdd ? oddItemColor : evenItemColor,
                            title: Text('Filleul $index'),
                          );
                        },
                      ),
                    ],
                  ),
                ),
              )
            ),
          ),
        ],
      ),
    );
  }
}
